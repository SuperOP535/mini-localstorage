# Mini LocalStorage

Minimal localStorage implementation for Node.js. Completely follows the spec (with the addition of a function).

**WARNING**: You cannot edit an object directly, for example `localStorage.myObject.hello = 1;` will NOT be saved to the disk.
Instead do `localStorage.__editObject('myObject', 'hello', 1);`, currently this function does not support editing deep objects.
OR you can simply edit it and call save() manually.

### Other notes:
- You cannot save anything non-JSON (for example functions, regex patterns etc.). There are ways around this like using .toString() and then loading.
- It's currently not possible to reload a database, a new MiniLocalStorage with the same path must be created.
- Using Object.defineProperty is not supported and will also not be supported because of the fact that it can be used to add custom functions.  
  Same applies to Object.getOwnPropertyDescriptor.
- Looping through the plain object is not possible, use localStorage.data to loop or getting all keys.

### Usage:
```js
const MiniLocalStorage = require('mini-localstorage');
const localStorage = MiniLocalStorage('./path-to-file.json');
localStorage.whatever = true;
```

The API is going to change `new MiniLocalStorage(path)` in future versions.

### Extending
To add your own functions you must use Object.defineProperty.